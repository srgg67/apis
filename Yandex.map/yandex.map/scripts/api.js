/**
 * Загрузить слайдер
 * @param nmb - индекс объекта
 */
function loadSliderBox(nmb){
    managePreview(nmb, 'slides')
}
/**
 * Загрузить видео
 * @param nmb - индекс объекта
 */
function loadVideoBox(nmb){
    managePreview(nmb, 'video')
}
//----------------------------------------------------------------
/**
 * Вернуть первоначальный фон меткам за исключением активной
 * @returns {HTMLElement}
 */
function dropBalloonsColors(index){
    //console.log('dropBalloonsColors');
    for(var i in myPlacemarks)
        if(i!==index)
            myPlacemarks[i].options.set('iconImageHref', getMarksBgImg());
}
/**
 * Получить селектор элемента закрытия балуна
 * @returns {*|jQuery|HTMLElement}
 */
function getBalloonCloser(){
    return document.querySelector('ymaps.ymaps-b-balloon__close');
}
/**
 * Получить контейнер загрузки контента
 * @param rollover
 * @returns {string}
 */
function getLoadedContentContainer(){
    return document.getElementById('loaded-content');
}
/**
 * Получить путь к файлу с фоном для метки
 * @param hide
 * @returns {HTMLElement}
 */
function getMarksBgImg(rollover){
    var file_name='assets/images/controls/roundImage';
    if(rollover) file_name+='2';
    file_name+='.png';
    return file_name;
}
/**
 * Получить блок с изображениями слайдера
 * @returns {HTMLElement}
 */
function getPixBox(){
    return document.getElementById('pix');
}
/*
 * Получить контейнер для загружаемого контента
 */
function getPreviewContainer(){
    return document.getElementById('preview-container');
}
/**
 * Получить iFrame с video
 */
function getVideoIFrame(loader){
    var ld='';
    if(loader){
        $.getScript("https://www.youtube.com/iframe_api", function(){
            //console.log('Youtube api is loaded!');
            $('#wait-for-video').fadeOut(200);
        });
        ld='<div class="wait_for_load" id="wait-for-video"></div>';
    }
    return ld+'<iframe width="100%" height="443" src="http://www.youtube.com/embed/krSOMotK7Qo" frameborder="0" allowfullscreen></iframe>';
}
/**
 * Получить затеняющий слой
 * @returns {*|jQuery|HTMLElement}
 */
function getShell(){
    return document.getElementById('shell');
}
