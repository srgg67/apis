var path = 'assets/cache/slides/',
    pics = [],
    imgs = [
        ['one.jpg', 'two.jpg', 'three.jpg', 'four.jpg', 'fife.jpg'],
        ['h2_1.jpg', 'h2_2.jpg', 'h2_3.jpg', 'h2_4.jpg', 'h2_5.jpg'],
        ['h3_1.jpg', 'h3_2.jpg', 'h3_3.jpg', 'h3_4.jpg', 'h3_5.jpg']
    ];
for (var i = 0, j = imgs.length; i < j; i++) {
    pics[i]=[];
    for(var ii= 0, jj=imgs[i].length; ii<jj; ii++){
        pics[i][ii] = new Image();
        pics[i][ii].src = path + imgs[i][ii];
    }
}