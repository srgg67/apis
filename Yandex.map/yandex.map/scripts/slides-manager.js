$(function () {
    //console.log(imgs);
    // slide images
    var loaded_content_str = '#loaded-content';
    // клик по кнопке видео на слайдере
    $(loaded_content_str).on('click', '#object-video', function (){
        var content_video_id = 'content-video',
            loadedContentContainer = getLoadedContentContainer();
        //console.dir(loadedContentContainer);
        //console.log('content_video_id size: '+$('#'+content_video_id,loadedContentContainer).size());
        // если видео ещё не было добавлено
        if(!$('#'+content_video_id,loadedContentContainer).size()){
            $(loadedContentContainer).append(/*
                Внешний контейнер для видео, загружаемого после клика
                по кнопке "Смотреть видео" на слайдере
            */'<div class="video outer_container" id="'+content_video_id+'">' + /*
                    Блок с видео (со встроенным iframe)
                */'<div id="loaded-content-video">'+
            getVideoIFrame(true)+'</div>'+/*
                    Кнопка "закрыть"
                */'<div class="close"></div>'+/*
           */'</div>');
            switchSliderOpacity(true);
        }
    });
});
/**
 * Обработать клик по указателю слайдера
 * @param direction
 */
function handleSlides(direction){
    var func = 'appendTo',
        sliderBoxId ='pix',
        sliderBox = document.getElementById(sliderBoxId),//
        // отступы от центральной картинки слайдера
        imgOffset = 128,
        // извлечь ширину картинок слайдера (532),
        step = document.querySelector('#images-container img').width,
        boxOffsetLeft = -(step-imgOffset),
        order = 'first';
    //console.group('%cimgOffset: '+imgOffset, 'color: violet');
    if(direction=='left'){
        imgOffset=-(step*2-imgOffset);
    }else if(direction=='right'){
        func = 'prependTo';
        //step *= -1;
        order = 'last';
    } //console.groupEnd();
    $(sliderBox).animate( // #pix
        {
            left: imgOffset+'px'
        },  300,
        function () {
            var sliderBoxSelector ='#'+sliderBoxId+' img',
             selName = sliderBoxSelector+':'+order+'-child';
             // append/prependTo
             $(selName)[func]($(sliderBox));
             // div#pix
             $(this).css({
                left: boxOffsetLeft+'px'
             });
        });
}