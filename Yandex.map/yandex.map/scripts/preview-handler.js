/**
 * Открыть слайдер
 * @param nmb
 * @param source_to_show
 */
function managePreview(nmb, source_to_show){
    //console.dir(source_to_show);
    var shell   = getShell(),
        previewContainer = getPreviewContainer();
    if(source_to_show || $(previewContainer).is(':visible')){
        // переключить дополнительные классы для контейнера с контентом
        $(previewContainer).removeClass((source_to_show=='slides')? 'video':'slides');
        $(previewContainer).addClass(source_to_show);
        //установить позицию блока предпросмотра
        var ofstLeft= (window.innerWidth-$(previewContainer).outerWidth())/ 2,
            ofstTop = (window.innerHeight-$(previewContainer).outerHeight())/ 2;
        // через jQuery не работает...
        previewContainer.style.top=ofstTop+'px';
        previewContainer.style.left=ofstLeft+'px';
        if(source_to_show){
            // блок для загружаемого контента
            var contentContainer = $('#loaded-content');
            // загрузить слайдер
            if(source_to_show=='slides'){
                // загрузить скрипт обработки вторичного блока с видео
                $.getScript("scripts/video-manager.js");
                // загрузить шаблон со слайдером, разместить картинки
                $(contentContainer).load('templates/slider.html', function(){
                    // загрузить данные объекта
                    $.getJSON("scripts/data/items"+(nmb+1)+".json", function( data ) {
                        //console.dir(data);
                        $.each( data, function( key, val ) {
                            //console.log(key, val);
                            if(key=="object-params"){
                                //console.dir(val);
                                for(var li in val){
                                    var currentLi =  $('#'+key+' li').eq(li); // li[0], li[1], li[2]
                                    $('span',currentLi).eq(0).html(val[li]["value"]);
                                    $('span',currentLi).eq(1).html("м<sup>2</sup>");
                                    $('>div',currentLi).html(val[li]["param"]);
                                    //console.dir(currentLi);
                                }
                            }else{
                                $('#'+key).html(val);
                            }
                        });
                        $('#object-number').html(nmb+1);
                    });
                    // загрузить данные изображений
                    $.getScript("scripts/data/slides.js", function() {
                        // загрузить скрипт обработки клика по указателям слайдера
                        $.getScript("scripts/slides-manager.js", function () {
                            var Pictures = '';
                            for (var im = 0, gs = pics[nmb].length; im < gs; im++) {
                                Pictures += pics[nmb][im].outerHTML;
                            }
                            $(getPixBox()).append(Pictures);
                        });
                    });
                });
            }else // загрузить видео
            if(source_to_show=='video'){
                //
                $(contentContainer).html(getVideoIFrame(true));
            }
            // затенить
            $(shell).fadeIn(200, function(){
                $(previewContainer).fadeIn(200);
            });
        }
    }
}